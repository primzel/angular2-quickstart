##Description.
    angular2 has two implimentations. one is with es5 and other is with es6
    however es6 also transformed to es5 after compilations. it is a front-end development mvc framwork implimentation.
    it gaves us object oreanted and modular way to write application, with very short time.
    angular set some basic rules to write apps.

    this application is written by following link https://angular.io/docs/ts/latest/tutorial/toh-pt1.html
##How To Setup.

1. Open terminal and issue command `sudo npm install -g typescript`
2. To install dependencies, `sudo npm install`
3. To start server. `npm start`
4. To compile typescript `npm tsc`, to run compiler with watch mode `npm tsc:w`
