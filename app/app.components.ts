///<reference path="../node_modules/angular2/typings/browser.d.ts"/>
import {Component} from "angular2/core";

export class Hero{
    id:number;
    name:string;
}



@Component({
    selector: 'my-app',
    template: '<input type="text" [(ngModel)]="title" /><h1>{{title}}</h1>'
})
export class AppComponent
{
    title="Tour of Heroes";
    hero: Hero={
        id:1,
        name:"Qasim"
    };
}